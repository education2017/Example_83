package hr.ferit.bruno.example_83;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.OnItemLongClick;

public class MainActivity extends Activity {

    @BindView(R.id.lvBooks) ListView lvBooks;
    @BindView(R.id.bAddBook) Button bAddBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ArrayList<Book> booksList = loadBooks();
        BookAdapter adapter = new BookAdapter(booksList);
        lvBooks.setAdapter(adapter);
    }

    private ArrayList<Book> loadBooks() {

        ArrayList<Book> books = new ArrayList<>();

        String[] authors = getResources().getStringArray(R.array.authors);
        String[] titles = getResources().getStringArray(R.array.titles);
        int[] years = getResources().getIntArray(R.array.years);
        String[] urls = getResources().getStringArray(R.array.urls);

        for (int i=0; i<authors.length; i++){
            books.add(new Book(authors[i], titles[i], years[i], urls[i]));
        }

        return books;
    }

    @OnItemLongClick(R.id.lvBooks)
    public boolean onListItemLongClick(int position){
        ((BookAdapter) this.lvBooks.getAdapter()).removeBook(position);
        return true;
    }

    @OnClick(R.id.bAddBook)
    public void onClick(View view){
        Book book = new Book("New book", "New author", 2017, "wwww.image.com");
        ((BookAdapter) this.lvBooks.getAdapter()).addBook(book);
    }
}
